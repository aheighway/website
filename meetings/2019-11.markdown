meeting_title: "taskwarrior"
meeting_datetime: 2019-11-12T19:30:00
meeting_speaker: "Tomas Babej"
meeting_location: "George Vari Engineering and Computing Centre, Room 203"
meeting_location_template: _locations/r-gvecc-203.html
meeting_schedule_template: _schedules/2015-10.html


Taskwarrior is an open-source, cross platform task management tool available right in your terminal!

In this talk, we will provide a comprehensive overview of its capabilities, usage tips and tricks, hook development, integrations with other tools (like vim), syncing and more.


Come join us to learn how to harness the power of the command line to declutter your todo lists.
